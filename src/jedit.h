#include <iostream>
#include <stdlib.h>

#ifndef JEDIT_H
#define JEDIT_H

class Jedit
{
public:
    Jedit();
    ~Jedit();
    std::string loadUser();
    void menu(char, std::string);
    void newNote(std::string);
    void deleteNote(std::string);
    void showNote(std::string);
    void editNote(std::string);
    void renameNote(std::string);
    void listNotes();
    void cloneNote(std::string);
    void options(char, std::string);

private:
    std::string user;
};
#endif //JEDIT_H
