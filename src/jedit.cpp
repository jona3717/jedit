#include "jedit.h"
#include <iostream>
#include <fstream> /* Trabajar con archivos de texto. */
#include <dirent.h> /* Visualisar directorios y su contenido. */
#include <cstring>
#include <chrono> /* Usar el cronómetro para poner temporizador al thread actual.*/
#include <thread> /* Trabajar con threads de procesos. */

using namespace std;

int main(int argc, char *argv[])
{
    char option;
    Jedit j;

    if(argc > 1){
        option = *argv[1];
    }
    switch ( argc ) {
        case 1: j.menu('0', "null");
            break;

        case 2: if(option != 'n' && option != 'l')
                    j.options(option, "null");
                else
                    j.menu(option, "null");
            break;

        case 3: if(option != 'n' && option != 'l')
                    j.options(option, argv[2]);
                else
                    j.menu(option, argv[2]);
            break;

        default: cout << "Error: Argumentos no válidos.";
            break;
    }				/* -----  end switch  ----- */

    return 0;
}

Jedit::Jedit()
{
}

Jedit::~Jedit()
{
}

/* 
 * ===  FUNCTION  ======================================================================
 *         Name: Jedit::loadUser
 *  Description: extrae el nombre de usuario 
 * =====================================================================================
 */
string Jedit::loadUser ()
{
    string username;
    char uname[20];
    system("whoami > ~/.jedit/user");
    ifstream file("/home/jonathan/.jedit/user");

    if(!file.fail()){
        while(!file.eof()){
            file.getline(uname, 20);
            username = uname;
            return username;
        }
    } else{
        cerr << "Fallo al abrir el archivo ./jedit/user";
        exit(1);
    }
    file.close();

    return username;
}		/* -----  end of function Jedit::loadUser  ----- */

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  Jedit::menu
 *  Description:  Muestra el menú inicial con las opciones de usuario.
 * =====================================================================================
 */
void Jedit::menu(char option, string name)
{
    if(option == 'n'){
        newNote(name);
    } else if(option == 'l'){
        listNotes();
    } else if(option == '0'){
        system("clear");
        cout << "*****************Jedit*****************" << endl;
        cout << "*                                     *" << endl;
        cout << "*1. Crear nota[n].                    *" << endl;
        cout << "*2. Listar notas[l].                  *" << endl;
        cout << "*3. Salir[q].                         *" << endl;
        cout << "*                                     *" << endl;
        cout << "***************************************" << endl;
    
        char selection;
        string arg;
        cin >> selection;
    
        switch (selection) {
            case 'n':	cout << "Nombre de la nota: ";
                        cin >> arg;
                        newNote(arg);
                        menu('0', "null");
                break;
    
            case 'l':	listNotes();
                break;

            case 'q':	exit(1);
                break;

            default:	cout << "Opción inválida." << endl;
                        std::this_thread::sleep_for (std::chrono::seconds(1));
                        menu('0', "null");
                break;
        }				/* -----  end switch  ----- */
    } else
        cout << "Menu: Opción inválida.";
}		/* -----  end of function Jedit::menu  ----- */


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  Jedit::newNote
 *  Description:  Crea una nueva nota y la edita con Vim
 * =====================================================================================
 */
void Jedit::newNote(string note)
{
    string fname = note;
    //-Verifica si se ha ingresado el nombre de la nota
    if(fname != "null" && fname != "q"){
        string command = "vim ~/.jedit/notes/" + fname;
        system(command.c_str());
    //-Crea el achivo con el nombre asignado y lo abre en el editor Vim
    } else if(fname == "q"){
        menu('0', "null");
    }else{    
        //-Crea el nombre del archivo.
        fflush(stdin);
        cout << "Ingresa el nombre de la nota [q para cancelar]:" << endl;
        cout << "-----------------------------------------------" << endl;
        cin >> fname;
        newNote(fname);
    }

}		/* -----  end of function Jedit::newNote  ----- */

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  Jedit::deleteNote
 *  Description:  Elimina la nota indicada
 * =====================================================================================
 */
void Jedit::deleteNote(string note)
{
    string command = note;
    /*-Verifica si se ha ingresado el nombre de la nota
     * Si se ingresó el nombre elimina la nota, de lo contrario
     * solicita el nombre de la nota a eliminar.*/
    if(command != "null" && command != "q"){
        command = "rm ~/.jedit/notes/" + command;
        system(command.c_str());
        listNotes();
    }else if(command == "q"){
        listNotes();
    } else{
        cout << "Nombre de la nota" << endl;
        cin >> command;
        deleteNote(command);
    }
}

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  Jedit::showNote
 *  Description:  Toma el nombre de la nota, la busca y la muestra.
 * =====================================================================================
 */
void Jedit::showNote(string note)
{
    string command = note;
    /* Verifica si se ha ingresado el nombre de la nota,
     * si se ingresó, muestra la nota, de lo contrario
     * solicita el nombre de la nota.'*/
    if(command != "null" && command != "q"){
        system("clear");
        command = "cat ~/.jedit/notes/" + command;
        system(command.c_str());
        system("read");
        listNotes();
    } else if(command == "q"){
        listNotes();
    }
    else{
        cout << "Nombre de la nota" << endl;
        cin >> command;
        showNote(command);
    }
}

void Jedit::editNote(string note)
{
    string command = note;
    /* Verifica si se ha ingresado el nombre de la nota,
     * si se ingresó, abre la nota en vim, de lo contrario
     * solicita el nombre de la nota.*/
    if(command != "null" && command != "q"){
        command = "vim ~/.jedit/notes/" + command;
        system(command.c_str());
        listNotes();
    } else if(command == "q"){
        listNotes();
    } else{
        cout << "Nombre de la nota" << endl;
        cin >> command; 
        editNote(command);
    }
}

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  Jedit::renameNote
 *  Description:  Busca la nota seleccionada y cambia el nombre actual por el indicado.
 * =====================================================================================
 */
void Jedit::renameNote(string note)
{
    string command = note, name;
    /* Verifica si se ha ingresado el nombre actual de
     * la nota y el nombre por el que se va a cambiar,
     * si se ingresó, realiza el cambio de nombre, de lo
     * contrario solicita el nombre de la nota*/
    if(command != "null" && command != "q"){
        cout << "Nuevo nombre: ";
        cin >> name;
        command = "mv ~/.jedit/notes/" + command + " ~/.jedit/notes/" + name;
        system(command.c_str());
        system("clear");
        listNotes();
    } else if(command == "q" || name == "q"){
        listNotes();
    } else{
        cout << "Nombre de la nota" << endl;
        cin >> command;
        renameNote(command);
    }
}		/* -----  end of function Jedit::renameNote  ----- */

/* 
 * ===  FUNCTION  ======================================================================
 *         Name: Jedit::cloneNote
 *  Description: Verifica si se ha ingresado el nombre de la nota, si se ingresó,
 *               realiza el duplicado de la nota, de lo contrario solicita el nombre.
 * =====================================================================================
 */
void Jedit::cloneNote(string note)
{
    string command = note;
    if(command != "null" && command != "q"){
        command = "cp ~/.jedit/notes/" + command + " ~/.jedit/notes/" + command + "2";
        system(command.c_str());
        listNotes();
    } else if(command == "q"){
        listNotes();
    } else{
        cout << "Nombre de la nota" << endl;
        cin >> command;
        cloneNote(command);
    }
}		/* -----  end of function Jedit::cloneNote  ----- */

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  Jedit::listNotes
 *  Description:  Lista las notas guardadas
 * =====================================================================================
 */
void Jedit::listNotes()
{
    //-Recorre los archivos contenidos en la carpeta
    system("clear");
    int counter = 0;
    user = loadUser();
    string path = "/home/" + user + "/.jedit/notes";
    if(DIR *dir = opendir(path.c_str())){
        while(dirent *element = readdir(dir)){
            string fname = element-> d_name;
            if(fname != "." && fname != ".."){
                counter++;
                string fname = element->d_name;
                cout << counter << ". " << fname << endl;
            }
        }
    }
    
    //-Comprueba que la carpeta no esté vacía, de lo contrario muestra un mensaje
    if(counter < 1){
        cout << "No hay notas." << endl;
        std::this_thread::sleep_for (std::chrono::seconds(1));
        menu('0', "null");
    } else{
        cout << "" << endl;
        cout << "*****************************" << endl;
        cout << "1. Ver[v].                  *" << endl;
        cout << "2. Editar[e].               *" << endl;
        cout << "3. Renombrar[r].            *" << endl;
        cout << "4. Eliminar[d].             *" << endl;
        cout << "5. Clonar[c].               *" << endl;
        cout << "6. Menú[m]                  *" << endl;
        cout << "7. Salir[q]                 *" << endl;
        cout << "*****************************" << endl;

        char option;
        cin >> option;
        if(option != 'q' && option != 'm'){
            string arg;
            cin >> arg;
            options(option, arg);
        } else if(option == 'q')
            exit(1);
        else if(option == 'm')
            menu('0', "null");
    }
}		/* -----  end of function Jedit::listNotes  ----- */

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  Jedit::options
 *  Description:  Ejecuta la función correspondiente, dependiendo de los argumentos que
 *                se le pasen
 * =====================================================================================
 */
void Jedit::options(char option, string arg1)
{
    switch ( option ) {
        case 'v':   showNote(arg1);
            break;

        case 'e':   editNote(arg1);
            break;

        case 'r':   renameNote(arg1);
            break;

        case 'd':	deleteNote(arg1);
            break;

        case 'c':	cloneNote(arg1);
            break;

        case 'm':   menu('0', "null");
            break;

        case 'q':   exit(1);
            break;

        default:	cout << "Opción inválida." << endl;
                        std::this_thread::sleep_for (std::chrono::seconds(1));
                        listNotes();
            break;
        }				/* -----  end switch  ----- */
}		/* -----  end of function Jedit::options  ----- */
