# Jedit

A simple and intuitive notes editor for UNIX terminal.

### How to install

```
make install
```
### How to uninstall 

```
make uninstall
```

### How to use

* Init application:
```
jedit
```
More info in the app menu.
#### Quick comands

* New note:
```
jedit n
```
* Edit a note
```
jedit e note-name / jedit e
```
* Rename a note
```
jedit r note-name / jedit r
```
* Delete a note
```
jedit d note name / jedit d
```
* Clone(duplicate) a note
```
jedit c note-name / jedit c
```


## Construido con 🛠️ 

* [C++](https://isocpp.org/) - El lenguaje utilizado para el desarrollo 
* [Vim](https://www.vim.org) - El editor de código utilizado

## Autores ✒️ 

* **Jonathan Córdova** - *Desarrollo y documentación* - [jona3717](https://gitlab.com/jona3717) 

## Licencia

Este proyecto está bajo la Licencia (GPLv3) - mira el archivo [LICENSE.md](LICENSE.md) para detalles.

## Expresiones de Gratitud

* Comenta a otros sobre este proyecto 
* Invita una cerveza   al desarrollador.  
* Da las gracias públicamente 邏. 
* etc. 



---

