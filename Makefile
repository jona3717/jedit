
CFLAGS = -g -Wall

# Compila programa principal
jedit: src/jedit.o
	gcc -o jedit src/jedit.o

#Compila dependencia jedit.o
src/jedit.o: src/jedit.cpp src/jedit.h

#Regla para eliminar archivos no necesarios
.PHONY: clean

clean:
	rm -rf src/*.o

#Recla que copia los archvos al sistema
install: jedit
	echo "Copiando archivos..."
	sudo mv jedit /usr/bin/
	mkdir -p ~/.jedit/notes
	whoami > ~/.jedit/user
	make clean
	echo "Terminado."

uninstall:
	echo "Removiendo archivos de instalación..."
	sudo rm /usr/bin/jedit
	echo "Eliminando datos de usuario"
	rm -rf ~/.jedit
	echo "terminado"

